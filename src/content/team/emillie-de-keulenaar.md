---
draft: false
name: "Emillie de Keulenaar"
title: "University of Groningen"
avatar:
  {
    src: "https://www.rug.nl/staff/e.v.de.keulenaar/w484-1064174-emillie-v-de-keulenaar-copy-crop-noexkkjo6.jpg?&fit=crop&w=280",
    alt: "Emillie de Keulenaar",
  }
publishDate: "2023-08-09 15:39"
---
