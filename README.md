# Automatic Classifiers for Peace

The automatic classifiers for peace group is an open-source community of researchers, organisations and individuals that are interested in aiding social media analysis through the automatic classification of social media discourse.

This repository contains the experiments, documentation, tutorials and source code that the group has developed.

UWebsite URL - https://index-automatic-classifiers-for-peace-3cd97d67fe59f1bdae5ecb350.gitlab.io/about/ 

Discord:  https://discord.gg/5ejyAGwv 


## Mission & Goals
The repository is aiming to be a space/tools/platform that can allow for the sharing of work
between different researchers and organisations that can collaborate in a way that is:
- publicly visible
- other people/organisations can easily join in
- allows for the sharing of data in a way that fits the sensitivity of the data
- allows for the formalisation of shared code that can lead to high-quality libraries
- work towards products/classification models could be publicly used


### Knowledge base ([wiki](https://gitlab.com/automatic-classifiers-for-peace/acfp/-/wikis/home))
A place where people can understand the ideas and theory about building automatic classifiers for
peace. With the idea that it can be a starting point for people that want to get involved but don't
yet have the knowledge to do so.